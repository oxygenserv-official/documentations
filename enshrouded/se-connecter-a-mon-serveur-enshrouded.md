---
description: Vous trouverez ici le tutoriel pour se connecter à votre serveur Enshrouded.
---

# 🖥️ Se connecter à mon serveur Enshrouded

## **Étape** 1 : Trouver l'adresse IP ou le nom du serveur

Pour commencer, vous devez connaître votre adresse IP ou le nom de votre serveur. Pour ce dernier, rendez-vous sur le dashboard du serveur, puis dans l'onglet "Paramètres" :

<figure><img src="../.gitbook/assets/image (5) (1).png" alt=""><figcaption></figcaption></figure>

### **Étape** 2 : Trouver le serveur sur le jeu

#### Pour une connexion par IP (recommandé)

Ouvrez l'application Steam et allez dans le menu "Afficher" puis "Serveurs de jeu" :

<figure><img src="../.gitbook/assets/image (2).png" alt=""><figcaption></figcaption></figure>

Une nouvelle fenêtre s'ouvre, cliquez sur "Favoris" puis sélectionnez le jeu "Enshrouded" et cliquez sur "+" pour ajouter le serveur à vos favoris :

<figure><img src="../.gitbook/assets/image (1) (1).png" alt=""><figcaption></figcaption></figure>

Une popup apparaît, entrez l'adresse IP de votre serveur sous le format suivant IP:port + 1.&#x20;

{% hint style="info" %}
Par exemple si l'adresse IP affichée sur votre dashboard est 12.34.56.78:15800, alors vous devrez mettre 12.34.56.78:15801.
{% endhint %}

&#x20;Entrez l'adresse dans la popup puis cliquez sur "OK" :

<figure><img src="../.gitbook/assets/image (2) (1).png" alt=""><figcaption></figcaption></figure>

Votre serveur devrait maintenant être visible dans la liste de vos favoris :

<figure><img src="../.gitbook/assets/image (5).png" alt=""><figcaption></figcaption></figure>

Et il devrait apparaître en premier dans la liste des serveurs sur Enshrouded (actualisez si vous aviez déjà le jeu ouvert) :

<figure><img src="../.gitbook/assets/image (4).png" alt=""><figcaption></figcaption></figure>

#### Félicitation 🎉 ! Vous venez de rejoindre votre serveur Enshrouded !

#### Pour une connexion avec le nom du serveur

Cliquez sur le bouton "Jouer" :

<figure><img src="../.gitbook/assets/image (29).png" alt=""><figcaption></figcaption></figure>

Rendez-vous dans la partie "Rejoindre" :

<figure><img src="../.gitbook/assets/image (27).png" alt=""><figcaption></figcaption></figure>

Filtrez par nom du serveur en cliquant sur "Chercher un nom de serveur" :

<figure><img src="../.gitbook/assets/image (1) (1) (1) (1).png" alt=""><figcaption></figcaption></figure>

Entrez le nom de votre serveur puis cliquez sur "Confirmer" :

<figure><img src="../.gitbook/assets/image (2) (1) (1) (1).png" alt=""><figcaption></figcaption></figure>

Votre serveur devrait apparaître dans la liste :

<figure><img src="../.gitbook/assets/image (3) (1) (1).png" alt=""><figcaption></figcaption></figure>

#### Félicitation 🎉 ! Vous venez de rejoindre votre serveur Enshrouded !
