---
description: >-
  Vous retrouverez ici le tutoriel pour mettre un nom de domaine sur son
  serveur.
---

# 💬 Mettre un nom de domaine sur son serveur

## **Étape 1** : Accéder à votre dashboard

Sur la page d'accueil du dashboard, cliquez sur le bouton "Obtenir un nom de domaine".

<figure><img src="../.gitbook/assets/Screenshot_619.png" alt=""><figcaption><p>Où trouver le bouton "Obtenir un nom de domaine"</p></figcaption></figure>

## **Étape 2** : Choisir un nom de domaine

Pour choisir un nom de domaine pour votre serveur, vous devez renseigner le nom que vous choisissez et cliquez sur "Créer".

<figure><img src="../.gitbook/assets/Screenshot_620.png" alt=""><figcaption><p>Comment créer son nom de domaine</p></figcaption></figure>

#### Félicitation 🎉 ! Vous venez de choisir un nom de domaine pour votre serveur !
