---
description: >-
  Vous retrouverez ici le tutoriel pour créer une sauvegarde de base de données
  sur votre serveur et la modifier.
---

# 💿 Créer une sauvegarde de base de données sur son serveur

## **Étape 1** : Accéder aux bases de données

Dans le tableau de bord d'Oxygenserv, recherchez l'onglet ou la section qui concerne les "Bases de données".

<figure><img src="../.gitbook/assets/Screenshot_569.png" alt=""><figcaption><p>Où trouver la section "bases de données"</p></figcaption></figure>

## **Étape 2** : Créer une sauvegarde

En cliquant sur les 3 petits points <img src="../.gitbook/assets/Screenshot_774.png" alt="" data-size="original">, vous trouverez le bouton "Créer une backup".

<figure><img src="../.gitbook/assets/Screenshot_570.png" alt=""><figcaption><p>Comment trouver le bouton "Créer une backup"</p></figcaption></figure>

## **Étape 3** : Configurer la sauvegarde

Pour configurer votre sauvegarde, rien de plus simple ! Il suffit de renseigner son nom.

<figure><img src="../.gitbook/assets/Screenshot_571.png" alt=""><figcaption><p>Comment configurer sa sauvegarde</p></figcaption></figure>

#### Félicitation 🎉 ! Vous venez de créer votre sauvegarde de bases de données pour votre serveur, vous n'aurez plus qu'a cliquer sur le bouton "Restaurer" ou "Télécharger" pour récupérer vos données
