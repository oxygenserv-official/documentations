---
description: >-
  Vous retrouverez ici le tutoriel pour créer une base de données sur votre
  serveur et la modifier.
---

# 📝 Créer et accéder à sa base de données

## **Étape 1** : Accéder aux bases de données

Dans le tableau de bord d'Oxygenserv, recherchez l'onglet ou la section qui concerne les "Bases de données".

<figure><img src="../.gitbook/assets/Screenshot_572.png" alt=""><figcaption><p>Où trouver la section "Bases de données"</p></figcaption></figure>

## **Étape 2** : Créer une base de données

Pour créer sa base de données, il faudra cliquer sur le bouton "Créer une base de données".

<figure><img src="../.gitbook/assets/Screenshot_573.png" alt=""><figcaption><p>Comment créer sa base de données</p></figcaption></figure>

## **Étape 3** : Configurer sa base de données

Pour configurer sa base de données, il faut juste lui renseigner un nom, et appuyer sur "Créer" !

<figure><img src="../.gitbook/assets/Screenshot_574.png" alt=""><figcaption><p>Comment configurer sa base de données</p></figcaption></figure>

## **Étape 4** : Se connecter automatiquement à sa base de données

Pour accéder à sa base de données, il faut simplement cliquer sur le petit engrenage sur la droite, vous devriez être connecté automatiquement.

<figure><img src="../.gitbook/assets/Screenshot_576.png" alt=""><figcaption><p>Comment accéder à sa base de données</p></figcaption></figure>

## **Étape 5** : Se connecter manuellement à sa base de données

Pour se connecter, vous avez la possibilité d'utiliser phpmyadmin à cette adresse : [https://phpmyadmin.oxygenserv.com/](https://phpmyadmin.oxygenserv.com/)

Il suffit de rentrer les informations présentes sur la page de dashboard, tel que le nom d'utilisateur et le mot de passe, et choisir le bon serveur selon l'adresse IP de votre base de données.

<figure><img src="../.gitbook/assets/Screenshot_577.png" alt=""><figcaption><p>Comment se connecter à sa base de données</p></figcaption></figure>

Vous pouvez également utiliser des logiciels tiers comme HeidiSQL ou Navicat en utilisant les mêmes identifiants.

#### Félicitation 🎉 ! Vous venez de créer votre bases de données pour votre serveur !
