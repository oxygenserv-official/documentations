---
description: Vous retrouverez ici le tutoriel pour modifier le mot de passe de son compte.
---

# 🛡 Changer le mot de passe de son compte

## **Étape 1** : Accéder à votre profil

Pour accéder à votre profil, il faut tout d'abord se rendre tout en haut à droite, et cliquer sur "Mon profil".

<figure><img src="../.gitbook/assets/Screenshot_578.png" alt=""><figcaption><p>Comment trouver la section "Mon profil"</p></figcaption></figure>

## **Étape 2** : Changer son mot de passe

Pour changer votre mot de passe, cliquez sur le bouton "Changer de mot de passe".

<figure><img src="../.gitbook/assets/Screenshot_579.png" alt=""><figcaption><p>Comment changer son mot de passe</p></figcaption></figure>

## **Étape 3** : Recevoir son code par Email

Vous allez maintenant recevoir un code à 4 chiffres par email qu'il faudra renseigner sur la page.

<figure><img src="../.gitbook/assets/Screenshot_580.png" alt=""><figcaption><p>Comment mettre son code reçu par email</p></figcaption></figure>

## **Étape 4** : Changer son mot de passe

Il vous reste maintenant à changer votre mot de passe, en oubliant pas de le confirmer une 2ème fois

<figure><img src="../.gitbook/assets/Screenshot_581 (1).png" alt=""><figcaption><p>Changer votre mot de passe</p></figcaption></figure>

#### Félicitation 🎉 ! Vous venez de modifier votre mot de passe, notez le quelque part pour ne pas l'oublier !
