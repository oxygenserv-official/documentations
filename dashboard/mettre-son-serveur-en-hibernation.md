---
description: >-
  L'hibernation sert à mettre en pause votre service, tout en conservant vos
  données. Vous ne paierez que le coût d'hébergement. Tant que votre service est
  en hibernation, ce dernier sera inutilisable.
---

# 🐯 Mettre son serveur en hibernation

## **Étape 1 : Accéder à la page abonnements**&#x20;

Pour accéder à la page abonnements, il faut se rendre sur "Mon Profil" à gauche de votre dashboard, puis sur "Mes abonnements".

<figure><img src="../.gitbook/assets/image_2024-02-06_215703325.png" alt=""><figcaption></figcaption></figure>

## **Étape 2 : Mise en hibernation**&#x20;

Cliquez sur les trois petits points à droite de votre service.&#x20;

<figure><img src="../.gitbook/assets/image_2024-02-06_220125588.png" alt=""><figcaption></figcaption></figure>

Sélectionnez ensuite "Mettre en hibernation".

<figure><img src="../.gitbook/assets/image_2024-02-06_220240560.png" alt=""><figcaption></figcaption></figure>

Vérifiez que toutes les informations soient exactes avant la mise en hibernation. Cette dernière s'activera au prochain renouvellement, seul le prix de l'hibernation vous sera facturée pour un montant de 0.50€ pour 10Gb de stockage.&#x20;

<figure><img src="../.gitbook/assets/image_2024-02-06_220653788.png" alt=""><figcaption></figcaption></figure>

Une fois activé, vous aurez les détails du paiement dans vos abonnements.

<figure><img src="../.gitbook/assets/image_2024-02-06_221329482.png" alt=""><figcaption></figcaption></figure>

Vous pouvez réactiver votre service à tout moment sur votre tableau de bord.
