---
description: >-
  Vous retrouverez ici le tutoriel pour trouver et partager vos logs sur votre
  serveur.
---

# 📑 Trouver et partager ses logs serveur

{% hint style="danger" %}
Il ne faut pas partager ses logs à n'importe qui, des informations importantes peuvent s'y trouver
{% endhint %}

## **Étape 1** : Accéder à votre console

Pour accéder à la console, allez sur la section "Console".

<figure><img src="../.gitbook/assets/Screenshot_582.png" alt=""><figcaption><p>Où trouver la section "Console"</p></figcaption></figure>

## **Étape 2** : Partager vos logs

Vous n'avez plus qu'a cliquer sur le bouton "Partager les logs".

<figure><img src="../.gitbook/assets/Screenshot_583.png" alt=""><figcaption><p>Comment partager ses logs</p></figcaption></figure>

## **Étape 3** : Partager le lien de vos logs

Vous n'avez plus qu'a copier le lien "Paste-Me", que vous pouvez partager librement.

<figure><img src="../.gitbook/assets/Screenshot_584.png" alt=""><figcaption><p>Comment envoyer son lien</p></figcaption></figure>

#### Félicitation 🎉 ! Vous venez de partager vos logs !
