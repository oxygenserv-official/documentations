---
description: >-
  Vous retrouverez ici le tutoriel pour accéder à tous les fichiers de votre
  serveur et comment les modifier.
---

# 📂 Accéder aux fichiers de son serveur

{% hint style="info" %}
FileZilla est un client FTP (File Transfer Protocol) populaire et convivial qui vous permet de vous connecter à des serveurs FTP pour transférer des fichiers. Dans cet article, nous vous guiderons à travers les étapes nécessaires pour accéder aux fichiers votre serveur en FTP à l'aide de FileZilla.
{% endhint %}

## Etape 1 : Téléchargement et installation de _FileZilla_

La première étape consiste à télécharger FileZilla à partir du site officiel ([https://filezilla-project.org/](https://filezilla-project.org/)) et à l'installer sur votre ordinateur. FileZilla est disponible pour Windows, macOS et Linux, vous pouvez donc choisir la version qui convient à votre système d'exploitation.

## Etape 2 : Collecte des informations de connexion

Avant de vous connecter à votre serveur FTP, vous devez collecter certaines informations, que vous pouvez obtenir dans la section "FTP" du panel dans "Mes identifiants". Les détails suivants sont nécessaires :

1. _Adresse du serveur FTP_ : Il s'agit de l'adresse IP ou du nom d'hôte de votre serveur FTP.
2. _Nom d'utilisateur_ : Il s'agit du nom d'utilisateur associé à votre compte FTP.&#x20;
3. _Mot de passe_ : Il s'agit du mot de passe qui correspond à votre compte FTP.

<figure><img src="../.gitbook/assets/Screenshot_760.png" alt=""><figcaption><p>Où trouver ses identifants de connexion FTP</p></figcaption></figure>

## Etape 3 : Lancement de FileZilla

Une fois que vous avez installé FileZilla, lancez le programme depuis votre ordinateur. Vous verrez une interface conviviale avec des sections séparées pour l'affichage local (votre ordinateur) et l'affichage distant (le serveur FTP).

<figure><img src="../.gitbook/assets/Screenshot_762.png" alt=""><figcaption><p>Voici a quoi ressemble la page d'acceuil de Filezilla</p></figcaption></figure>

## Etape 4 : Remplissez les informations de connection à partir des informations fournies sur le dashboard&#x20;

<figure><img src="../.gitbook/assets/Screenshot_764.png" alt=""><figcaption><p>Où trouver les informations</p></figcaption></figure>

<figure><img src="../.gitbook/assets/Screenshot_763.png" alt=""><figcaption><p>Où mettre les informations</p></figcaption></figure>

## Etape 5 : Transfert de fichiers

Maintenant que vous êtes connecté à votre serveur FTP, vous pouvez transférer des fichiers entre votre ordinateur local et le serveur distant. Voici comment procéder :

1. Navigation dans les répertoires : Utilisez les panneaux "Affichage local" et "Affichage distant" pour naviguer dans les répertoires de votre ordinateur local et du serveur FTP respectivement. Cliquez sur les dossiers pour les ouvrir et afficher leur contenu.
2. Transfert de fichiers du serveur vers votre ordinateur : Pour télécharger un fichier du serveur FTP vers votre ordinateur, faites un clic droit sur le fichier souhaité dans la section "Affichage distant" et sélectionnez "Télécharger". Le fichier sera alors transféré vers le répertoire actif dans la section "Affichage local".
3. Transfert de fichiers de votre ordinateur vers le serveur : Pour envoyer un fichier de votre ordinateur local vers le serveur FTP, faites un clic droit sur le fichier dans la section "Affichage local" et sélectionnez "Envoyer". Le fichier sera alors transféré vers le répertoire actif dans la section "Affichage distant".
4. Gestion des transferts : FileZilla affiche les transferts en cours dans l'onglet "Transferts" en bas de la fenêtre. Vous pouvez mettre en pause, reprendre ou annuler des transferts individuels en utilisant les icônes correspondantes.

<figure><img src="../.gitbook/assets/Screenshot_765.png" alt=""><figcaption></figcaption></figure>

#### Félicitations [🎉](https://emojipedia.org/party-popper/) ! Vous savez maintenant comment vous connecter à votre serveur FTP à l'aide de FileZilla et transférer des fichiers entre votre ordinateur et le serveur distant.
