# 🤖 Inviter OxyBot et le configurer sur son discord

## **Étape 1 : Inviter le bot sur son serveur**

Pour commencer, connecte-toi à ton profil sur le **Dashboard**. Sous l'onglet "Mes intégrations", sélectionne "Configuration d'OxyBot". Une fenêtre pop-up apparaîtra, te fournissant toutes les informations nécessaires pour procéder.

Si tu préfères une méthode plus rapide, tu peux également inviter Oxybot directement en utilisant ce lien. Suivre [ce lien](https://discord.com/oauth2/authorize?client\_id=1107672334590025878\&scope=bot\&permissions=537201728) te permettra d'ajouter le bot à ton serveur immédiatement.

<figure><img src="../.gitbook/assets/vswjXlfkKU.png" alt=""><figcaption></figcaption></figure>

## **Étape 2 :** Trouver sa clé api

Ta clé API est essentielle pour connecter ton compte au bot Discord. Tu la trouveras dans la fenêtre pop-up que tu as ouverte précédemment, spécifiquement dans la section "Profil" de ton dashboard. Cette clé permet à Oxybot de récupérer et de gérer les informations de ton serveur efficacement.

{% hint style="danger" %}
Ne donnez surtout pas votre clé, elle doit rester personnelle, sinon d'autres personnes pourrait gérer les serveurs liés à votre compte à votre place
{% endhint %}

<figure><img src="../.gitbook/assets/bFDqB9KU0m.png" alt=""><figcaption></figcaption></figure>

## **Étape 3**: Configurer le bot

Maintenant, il faut donc configurer le bot, pour ce faire, entrez la commande `/setup api` sur le discord que tu veux configurer, une fenêtre devrait apparaître et entrez-y la clé que vous avez obtenue à l'étape précédente.&#x20;

<figure><img src="../.gitbook/assets/Discord_20nzaBmHHP.png" alt=""><figcaption></figcaption></figure>

Avec la commande `/setup language` vous pouvez choisir le choix de langue du bot entre le **français** et **l'anglais**.



## **Étape 4**: Configurer les permissions&#x20;

> Lorsque tu ajoutes Oxybot à ton serveur, toutes les commandes  sont restreintes par défaut pour éviter les mauvaises manipulations. Par exemple, nous limitons l'accès à des commandes critiques comme le redémarrage du serveur ou les modifications des informations de connexion FTP et DB. Cela prévient les situations où un nouveau membre pourrait accidentellement (ou intentionnellement) perturber le fonctionnement de ton serveur.

Discord donne la possibilité de configurer avec précision les permissions d'un bot discord. Pour accéder à l'interface de configuration, il suffit d'aller dans les **Paramètres du serveur** puis dans **Intégrations.** Sélectionne le bot OxyBot et appuie sur le bouton **Gérer.**

<figure><img src="../.gitbook/assets/245LM88DgE (1).png" alt=""><figcaption></figcaption></figure>

L'interface qui va s'ouvrir te permet de contrôler et de modifier les permissions d'accès au bot sur le discord : les salons, les utilisateurs et les commandes. Les paramètres en violet s'appliquent sur tout le discord et les paramètres en bleu s'appliquent que pour la commande paramétrée.

<figure><img src="../.gitbook/assets/zFZTYWdYEb (2).png" alt=""><figcaption></figcaption></figure>

Comme précisé précédemment, le rôle @everyone est caduque, car toutes les commandes sont bloquées pour les utilisateurs non-admin ("Manage Channel").&#x20;

Cependant, tu **peux autoriser l'utilisation des commandes uniquement dans certains salons**, cela peut être pratique si le bot est hébergé sur un discord communautaire.&#x20;



### Comment faire pour donner l'accès à une commande à un utilisateur non-admin ?

Tu peux modifier les accès à une commande en plus de la valeur par défaut (administrateur). Dans la configuration suivante, tous les utilisateurs avec la permission "modo" **ou** avec la permission "Administrateur" pourront utiliser la commande `/restart.`

<figure><img src="../.gitbook/assets/Discord_RRy6A8CggS (2).png" alt=""><figcaption></figcaption></figure>

Si tu souhaites approfondir la configuration des permissions, je t'invite à lire le tutoriel officiel de discord [ici](https://support.discord.com/hc/fr/articles/4644915651095-Permissions-de-Commandes).



#### La configuration est terminée !

Vous n'avez plus qu'à tester le bot en effectuant la première commande `/server` pour obtenir toutes les informations essentielles de son serveur. Si vous avez plusieurs serveurs, la commande `/servers` permet d'obtenir en un seul coup d'oeil une vision globale de vos services !&#x20;

{% hint style="info" %}
**Oxybot est actuellement en version 1.0, des petits bugs peuvent encore se présenter. Si tu rencontres des problèmes ou des irrégularités, n'hésite pas à les rapporter. Cela nous aidera à améliorer Oxybot pour tous ses utilisateurs :)**
{% endhint %}
