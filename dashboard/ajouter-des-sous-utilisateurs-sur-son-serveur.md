---
description: >-
  Vous retrouverez ici le tutoriel pour ajouter des sous-utilisateurs sur votre
  serveur et les modifier.
---

# 👥 Ajouter des sous-utilisateurs sur son serveur

## Etape 1 : Accédez à l'onglet de gestion des utilisateurs

Sur le dashboard, recherchez l'onglet ou la section qui concerne la "Gestion des utilisateurs".

<figure><img src="../.gitbook/assets/Screenshot_766.png" alt=""><figcaption><p>Où trouver la section "Gestion des utilisateurs"</p></figcaption></figure>

## Etape 2 : Création d'un nouveau sous-utilisateur

Une fois que vous êtes dans la section de gestion des utilisateurs, cherchez un bouton ou une option indiquant "Créer un nouvel utilisateur". Cliquez dessus pour commencer le processus de création d'un sous-utilisateur.

<figure><img src="../.gitbook/assets/Screenshot_767.png" alt=""><figcaption><p>Où trouver le bouton "Créer un nouvel utilisateur"</p></figcaption></figure>

## Etape 3 : Remplissage des informations du sous-utlisateur

Dans le formulaire de création d'utilisateur, vous devrez fournir les informations nécessaires pour configurer le compte du sous-utilisateur. Cela peut inclure les détails suivants :

1. Email de l'utilisateur : Renseignez l'email utilisé par le sous-utilisateur.
2. Privilèges d'accès : Sélectionnez les privilèges et les autorisations que vous souhaitez accorder au sous-utilisateur.

<figure><img src="../.gitbook/assets/Screenshot_768.png" alt=""><figcaption><p>Comment remplir la page d'informations</p></figcaption></figure>

### Etape 4 : Validation et création du sous-utilisateur&#x20;

#### Une fois que vous avez rempli toutes les informations nécéssaires, vérifiez les attentivement pour vous assurer de leur exactitudes. Ensuite, cliquez le bouton "Créer" ou "Valider" pour créer le sous-utilisateur.

<figure><img src="../.gitbook/assets/Screenshot_769.png" alt=""><figcaption><p>Appuyez sur le bouton Créer</p></figcaption></figure>

#### Félicitations [🎉](https://emojipedia.org/party-popper/) ! Vous avez appris à créer des sous-utilisateurs sur le tableau de bord d'Oxygenserv
