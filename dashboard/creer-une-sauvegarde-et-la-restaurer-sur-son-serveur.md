---
description: >-
  Vous retrouverez ici le tutoriel pour créer une sauvegarde sur votre serveur
  et la modifier.
---

# 💾 Créer une sauvegarde et la restaurer sur son serveur

{% hint style="danger" %}
_Vous n'avez accès qu'à un nombre limité de sauvegardes, vous pouvez supprimer une ancienne sauvegarde si besoin._
{% endhint %}

## **Étape 1** : Accédez aux sauvegardes

Dans le tableau de bord d'Oxygenserv, recherchez l'onglet ou la section qui concerne les "Sauvegardes".

<figure><img src="../.gitbook/assets/Screenshot_778.png" alt=""><figcaption><p>Où trouver la section "Sauvegardes"</p></figcaption></figure>

## **Étape 2** : Créer une sauvegarde

Pour créer une sauvegarde, vous devez cliquer sur le bouton "Créer une sauvegarde"

<figure><img src="../.gitbook/assets/Screenshot_779.png" alt=""><figcaption><p>Comment créer une sauvegarde</p></figcaption></figure>

## **Étape 3** : Configurer la sauvegarde

Pour configurer votre sauvegarde, il faudra renseigner le nom de la sauvegarde, les dossiers qu'il doit ignorer, et si la sauvegarde doit être vérouillée ou non. Pour valider votre sauvegarde, il faut appuyer sur le bouton "Créer"

<figure><img src="../.gitbook/assets/Screenshot_780.png" alt=""><figcaption><p>Configurer votre sauvegarde</p></figcaption></figure>

#### Félicitation 🎉 ! Vous venez de créer votre sauvegarde de fichiers pour votre serveur, vous n'aurez plus qu'a cliquer sur le bouton "Restaurer" pour récupérer vos fichiers
