---
description: >-
  Vous retrouverez ici le tutoriel pour planifier des tâches sur votre serveur
  et les modifier.
---

# 📋 Planifier une nouvelle tâche

## **Étape 1** : Accédez à la planification des tâches

Dans le tableau de bord d'Oxygenserv, recherchez l'onglet ou la section qui concerne le "Planificateur de tâches".

<figure><img src="../.gitbook/assets/Screenshot_770.png" alt=""><figcaption><p>Où trouver la section "Planificateur de tâches"</p></figcaption></figure>

## **Étape 2** : Créez une nouvelle tâche

Pour créer une nouvelle tâche, il vous suffit de cliquer sur le bouton "Créer une nouvelle planification".

<figure><img src="../.gitbook/assets/Screenshot_771.png" alt=""><figcaption><p>Où trouver le bouton "Créer une nouvelle planification"</p></figcaption></figure>

## **Étape 3** : Configurer la planification

Vous devez maintenant configurer votre planification, en renseignant son nom, les jours d'exécution, son heure, son activation. Dans notre exemple, nous allons créer un redémarrage journalier. Une fois terminé, il faut donc cliquer sur "Créer".

<figure><img src="../.gitbook/assets/Screenshot_772.png" alt=""><figcaption><p>Comment remplir la planification</p></figcaption></figure>

## **Étape 4** : Créer une nouvelle tâche

Ensuite, il faudra assigner une tâche à cette planification, en cliquant sur les 3 petits points ![](../.gitbook/assets/Screenshot_774.png), et "Nouvelle tâche".

<figure><img src="../.gitbook/assets/Screenshot_773.png" alt=""><figcaption><p>Où trouver le bouton "Nouvelle tâche"</p></figcaption></figure>

## **Étape 5** : Configurer la tâche

Pour configurer la tâche, vous devez renseigner son Action, le délai entre les tâches, et la commande associée, dans notre cas, le redémarrage du serveur. Pour finir, cliquez sur le bouton "Créer".

<figure><img src="../.gitbook/assets/Screenshot_776.png" alt=""><figcaption><p>Comment configurer la tâche</p></figcaption></figure>

#### Félicitation [🎉](https://emojipedia.org/party-popper/) ! Vous venez de créer une nouvelle planification pour votre serveur !&#x20;
