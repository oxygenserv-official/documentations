---
description: >-
  Vous retrouverez ici le tutoriel pour contacter le support en cas de questions
  ou problèmes
---

# 📞 Contacter le support (discord et dashboard)

## **Étape 1** : Accéder à la page support

Pour accéder à la page support, il faut cliquer sur le bouton "Assitance Technique" en bas à gauche du dashboard.

<figure><img src="../.gitbook/assets/image (7).png" alt=""><figcaption><p>Où trouver la section "Support" dans le dashboard</p></figcaption></figure>

## **Étape 2** : Créer un ticket

Pour créer un ticket, cliquez sur le bouton "Créer un ticket".

<figure><img src="../.gitbook/assets/Screenshot_589.png" alt=""><figcaption><p>Où trouver le bouton "Créer un ticket"</p></figcaption></figure>

## **Étape 3** : Configurer un ticket

Pour configurer un ticket, il faut simplement renseigner, le motif du problème, un titre, et sa description, en précisant parfois le serveur impacté, tout en restant poli ;).

<figure><img src="../.gitbook/assets/Screenshot_590.png" alt=""><figcaption><p>Comment créer son ticket</p></figcaption></figure>

Vous pouvez également nous contacter sur discord [https://discord.oxygenserv.com](https://discord.oxygenserv.com) dans le salon #questions-techniques.

#### Félicitation 🎉 ! Vous venez de Créer un ticket support !
