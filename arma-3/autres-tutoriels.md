# Autres tutoriels

Vous trouverez un lien avec une liste de tutoriels que notre partenaire officiel "The-Programmer" à réalisé.

{% embed url="https://forum.the-programmer.com/pages/arma-scripts/" %}

Vous y retrouverez des tutoriels sur des sujets de base, et également sur leurs scripts.
