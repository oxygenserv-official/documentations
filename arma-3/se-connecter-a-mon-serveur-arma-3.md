---
description: Vous trouverez ici le tutoriel pour se connecter à votre serveur Arma 3.
---

# 🖥 Se connecter à mon serveur Arma 3

## **Étape** 1 : Trouver l'adresse IP ou le nom du serveur

Pour commencer, vous devez connaître le nom de votre serveur ou votre adresse IP. Pour cela, rendez-vous sur le dashboard du serveur et trouvez en bas "Adresse IP" :

<figure><img src="../.gitbook/assets/image (10).png" alt=""><figcaption><p>Vision du dashboard oxygenserv.com</p></figcaption></figure>

Une fois que vous avez, trouvé l'adresse IP ou configuré un [nom de domaine](../dashboard/mettre-un-nom-de-domaine-sur-son-serveur.md), rendez-vous sur Arma 3.

### **Étape** 2 : Trouver le serveur sur le jeu

Rendez vous sur l'onglet "multijoueur", puis "Navigateur de serveurs" :

<figure><img src="../.gitbook/assets/image (6) (1) (1).png" alt=""><figcaption></figcaption></figure>

#### Pour une connexion par IP

Allez dans l'onglet "Connexion" et renseignez l'adresse IP et le port de votre serveur, sur votre dashboard, l'adresse est sous la forme "IP:port", vous devrez séparer les 2 parties pour les mettre dans leurs cases respectives. Et ensuite faites "Rejoindre" :

<figure><img src="../.gitbook/assets/image (7) (1).png" alt=""><figcaption></figcaption></figure>

#### Pour une connexion avec le nom du serveur

Allez dans l'onglet "Internet" -> "filtrer" :

<figure><img src="../.gitbook/assets/image (8).png" alt=""><figcaption></figcaption></figure>

Un onglet "option filtrage" s'ouvre et vous donne plusieurs choix de filtres. Dans la partie "Hôte", renseignez le nom de votre serveur et validez :

<figure><img src="../.gitbook/assets/image (9).png" alt=""><figcaption></figcaption></figure>

#### Félicitation 🎉 ! Vous venez de rejoindre votre serveur Arma 3 !
