---
description: Vous apprendrez dans ce tuto à avoir les permissions administrateur.
---

# 🛡️ Être administrateur sur mon serveur Arma 3



## **Étape** 1 : Accéder à la base de données

Pour commencer, il faut avoir un accès à la base de données du serveur. Cliquez sur l'onglet "Base de données" :

<figure><img src="../.gitbook/assets/image (4) (1) (1) (1) (1) (1).png" alt=""><figcaption></figcaption></figure>

Et sur le petit symbole paramètre :

<figure><img src="../.gitbook/assets/image (1) (1) (1) (1) (1) (1) (1) (1) (1).png" alt=""><figcaption></figcaption></figure>

## **Étape** 2 : Modifier son adminlevel

Une fois sur la base de données, rendez-vous dans la table "_players_", où vous trouverez tous les joueurs ayant rejoins votre serveur.

{% hint style="info" %}
Si vous n'y aparaissez pas, vous devez vous connecter au moins une fois au serveur, puis ressayer.
{% endhint %}

Dans la colonne "adminlevel", choisissez le niveau administratrateur du joueur (entre 0 et 5), 5 étant le niveau maximal :

<figure><img src="../.gitbook/assets/image (2) (1) (1) (1) (1) (1) (1) (1).png" alt=""><figcaption><p>Table des joueurs sur la base de données</p></figcaption></figure>

## **Étape 3** : Ouvrir le menu admin en jeu

Vous pouvez maintenant vous reconnecter au jeu, et vous devriez voir apparaître le bouton "Menu admin" dans votre menu Y :

<figure><img src="../.gitbook/assets/image (3) (1) (1) (1) (1) (1) (1).png" alt=""><figcaption></figcaption></figure>

#### Félicitation 🎉 ! Vous êtes maintenant administrateur de votre serveur Arma 3 !
