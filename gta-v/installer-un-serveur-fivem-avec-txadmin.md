---
description: >-
  Vous retrouverez ici le tutoriel pour installer votre serveur FiveM avec
  TXAdmin.
---

# ⚙ Installer un serveur FiveM avec TXAdmin

## **Étape 1** : Accéder à la section "Installation"

Pour commencer, nous allons installer FiveM sur notre serveur, pour ce faire, il faut aller dans la section "Installation" du dashboard, et appuyer sur le bouton "Installer" à côté de FiveM.

<figure><img src="../.gitbook/assets/Screenshot_591.png" alt=""><figcaption><p>Où trouver la section "Installation" du dashboard</p></figcaption></figure>

## **Étape 2** : Générer votre clé FiveM

Vous devrez d'abord vous rendre sur le site [https://keymaster.fivem.net/](https://keymaster.fivem.net/), et vous connecter.

<figure><img src="../.gitbook/assets/Screenshot_593.png" alt=""><figcaption><p>Comment se connecter</p></figcaption></figure>

## **Étape 3** : Générer votre clé FiveM

Une fois connecté, vous devez enregistrer un nouveau serveur, en précisant son nom, l'ip que vous pouvez trouver sur le dashboard (Enlever le port à cette IP). **A noter : vous devez choisir la catégorie VPS** et mettre le nom d'Oxygenserv.

<div>

<figure><img src="../.gitbook/assets/Screenshot_594.png" alt=""><figcaption><p>Comment enregistrer un nouveau serveur</p></figcaption></figure>

 

<figure><img src="../.gitbook/assets/Screenshot_595.png" alt=""><figcaption><p>Comment trouver l'ip de son serveur</p></figcaption></figure>

</div>

<figure><img src="../.gitbook/assets/Screenshot_598.png" alt=""><figcaption><p>Comment renseigner les informations de son serveur</p></figcaption></figure>

## **Étape 4** : Continuer l'installation

Vous devez choisir votre version de FiveM, qui dépendra de la version où vos plugins / mods sont, et renseigner votre clé, si vous en avez déjà une, vous devez la refaire avec l'ip de votre serveur. Maintenant vous pouvez renseigner votre clé et attendre que votre serveur démarre automatiquement.

<figure><img src="../.gitbook/assets/Screenshot_597.png" alt=""><figcaption><p>Comment installer son serveur</p></figcaption></figure>

## **Étape 5** : Installer TXAdmin

Pour installer TXAdmin, il faut cliquer sur le bouton TXAdmin sur la page d'accueil du dashboard.

<figure><img src="../.gitbook/assets/Screenshot_599.png" alt=""><figcaption><p>Où trouver le bouton "TXAdmin"</p></figcaption></figure>

## **Étape 6** : Démarrer son serveur

Une fois le bouton installer TXAdmin fait, il faut démarrer son serveur, et se diriger dans la section "Console" du dashboard.

<figure><img src="../.gitbook/assets/Screenshot_600.png" alt=""><figcaption><p>Où trouver la section "Console" du dashboard</p></figcaption></figure>

## **Étape 7** : Configurer TXAdmin

Pour configurer TXAdmin, il faut cliquer sur le bouton "TXAdmin", en haut à droite du dashboard comme précédemment. Ensuite, il faut renseigner le code PIN, qui est renseigné sur la console de votre serveur sous la phrase _**Use the PIN below to register:**_.

<figure><img src="../.gitbook/assets/Screenshot_601 (1).png" alt=""><figcaption><p>Comment trouver la page TXAdmin</p></figcaption></figure>

## **Étape 8** : Lier son compte FiveM avec TXAdmin

Vous devez maintenant autoriser TXAdmin avec FiveM.

<figure><img src="../.gitbook/assets/Screenshot_602.png" alt=""><figcaption><p>Comment lier son compte FiveM</p></figcaption></figure>

## **Étape 9** : Créer le mot de passe admin

Dans cette étape, vous devez renseigner le mot de passe admin que vous devez noter quelque part et ne surtout pas perdre.

<figure><img src="../.gitbook/assets/Screenshot_603.png" alt=""><figcaption><p>Créer son mot de passe admin</p></figcaption></figure>

## **Étape 10** : Configurer TXAdmin

Pour commencer, vous devez choisir un nom pour votre serveur.

<figure><img src="../.gitbook/assets/Screenshot_604.png" alt=""><figcaption><p>Choisir le nom de son serveur</p></figcaption></figure>

## **Étape 11** : Choisir la template de son serveur

Vous pouvez donc maintenant choisir la base de votre serveur, nous vous recommandons de choisir Popular Recipes et ensuite ESX, mais vous pouvez toujours importer la votre.

<figure><img src="../.gitbook/assets/Screenshot_605.png" alt=""><figcaption><p>Choisir sa template</p></figcaption></figure>

## **Étape 12** : Le data location

A cette étape, il ne faut pas changer le dossier.

<figure><img src="../.gitbook/assets/Screenshot_606.png" alt=""><figcaption><p>Ne pas changer le dossier</p></figcaption></figure>

## **Étape 13** : Server Deployer

Maintenant vous devez arriver sur une nouvelle page, pour la première étape, veuillez ne rien toucher.

<figure><img src="../.gitbook/assets/Screenshot_607.png" alt=""><figcaption><p>Server Deployer</p></figcaption></figure>

## **Étape 14** : Remettre sa clé FiveM

Ensuite, vous devez reprendre la clé CFX de FiveM et la renseigner une nouvelle fois et **passez à l'étape 15 du guide sans valider**.

<figure><img src="../.gitbook/assets/Screenshot_608.png" alt=""><figcaption><p>Comment renseigner sa clé CFX</p></figcaption></figure>

## **Étape 15** : Renseigner la base de données

Pour renseigner votre base de données, cliquez sur "Show/Hide Database options (advanced)", pour créer une base de donnée, [consultez le tutoriel ICI](https://docs.oxygenserv.com/global/creer-une-sauvegarde-de-base-de-donnees-sur-son-serveur). Après avoir rempli les informations, cliquez sur "Run"

<figure><img src="../.gitbook/assets/Screenshot_609.png" alt=""><figcaption><p>Comment remplir les informations de la base de données</p></figcaption></figure>

## **Étape 16** : Configurer le fichier "server.cfg"

Il faut maintenant configurer votre fichier "server.cfg" avec les informations mises sur la page.

<figure><img src="../.gitbook/assets/Screenshot_610.png" alt=""><figcaption><p>Où trouver le fichier "server.cfg"</p></figcaption></figure>

<figure><img src="../.gitbook/assets/Screenshot_611.png" alt=""><figcaption><p>Comment configurer le fichier "server.cfg"</p></figcaption></figure>

## **Étape 17** : Finir l'installation

Maintenant vous n'avez plus qu'a retourner sur la page TXAdmin et cliquer sur "Save & Run Server". Et voilà ! votre serveur est prêt !

<figure><img src="../.gitbook/assets/Screenshot_612.png" alt=""><figcaption><p>Fin de l'installation</p></figcaption></figure>

#### Félicitation 🎉 ! Vous venez d'Installer votre serveur FiveM avec TXAdmin !
