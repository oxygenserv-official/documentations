---
layout:
  title:
    visible: true
  description:
    visible: false
  tableOfContents:
    visible: true
  outline:
    visible: true
  pagination:
    visible: true
---

# 😤 Corriger les erreurs "1";"2";"126";"127" et "134"

{% hint style="info" %}
Ces codes d'erreurs surviennent lorsque votre dossier "Alpine" n'existe pas, ou est corrompu.
{% endhint %}

## **Étape 1** : Allez dans la catégorie "Installation" sur le dashboard

<figure><img src="../.gitbook/assets/image (4) (1) (1) (1) (1) (1) (1).png" alt=""><figcaption></figcaption></figure>

## **Étape 2** :  Cliquez sur "changer version"

<figure><img src="../.gitbook/assets/image (1) (1) (1) (1) (1) (1) (1) (1) (1) (1).png" alt=""><figcaption></figcaption></figure>

## **Étape 3** : Sélectionnez la première version proposée

<figure><img src="../.gitbook/assets/image (2) (1) (1) (1) (1) (1) (1) (1) (1).png" alt=""><figcaption></figcaption></figure>

{% hint style="danger" %}
Ne pas activer la suppression des fichiers existants. \
Avec cela, l'action est irréversible et vous risquez de perdre toutes vos données !&#x20;
{% endhint %}

<figure><img src="../.gitbook/assets/image (3) (1) (1) (1) (1) (1) (1) (1).png" alt=""><figcaption></figcaption></figure>

Enfin, cliquez sur le buton "changer de version" puis patientez :

<figure><img src="../.gitbook/assets/image (4) (1) (1) (1) (1) (1) (1) (1).png" alt=""><figcaption><p>Après cela, démarez votre serveur.</p></figcaption></figure>
