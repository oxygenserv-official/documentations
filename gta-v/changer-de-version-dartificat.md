---
description: >-
  Résolvez efficacement l'erreur 'Server Artifact version is outdated' sur votre
  serveur FiveM avec notre tutoriel simple et direct. Découvrez les étapes clés
  pour mettre à jour votre serveur.
layout:
  title:
    visible: true
  description:
    visible: false
  tableOfContents:
    visible: true
  outline:
    visible: true
  pagination:
    visible: true
---

# ↗️ Changer de version d'Artificat

## **Étape 1** : Allez dans la catégorie "Installation" sur le dashboard

> En cas d'incertitude ou si vous ne vous sentez pas à l'aise avec les manipulations techniques, il est fortement recommandé de réaliser une sauvegarde complète de votre serveur avant de procéder à toute modification. Cette précaution vous assure de pouvoir restaurer votre serveur à son état initial en cas de problème.

<figure><img src="../.gitbook/assets/image (4) (1) (1) (1) (1) (1) (1).png" alt=""><figcaption></figcaption></figure>

## **Étape 2** :  Cliquez sur "changer version"

<figure><img src="../.gitbook/assets/image (1) (1) (1) (1) (1) (1) (1) (1) (1) (1).png" alt=""><figcaption><p>La version actuelle affiche la version actuelle de votre Artificat.</p></figcaption></figure>

## **Étape 3** : Sélectionnez la nouvelle version souhaitée

<figure><img src="../.gitbook/assets/image (2) (1) (1) (1) (1) (1) (1) (1) (1).png" alt=""><figcaption></figcaption></figure>

Il est possible d'installer n'importe quelle version de votre choix, mais FiveM suggère l'utilisation d'une version qualifiée de 'stable'. De plus, si vous le souhaitez, vous pouvez opter en toute confiance pour la dernière version disponible, qui permet également de corriger l'erreur.

{% hint style="danger" %}
Ne pas activer la suppression des fichiers existants. \
Avec cela, l'action est irréversible et vous risquez de perdre toutes vos données !&#x20;
{% endhint %}

<figure><img src="../.gitbook/assets/image (3) (1) (1) (1) (1) (1) (1) (1).png" alt=""><figcaption></figcaption></figure>

Enfin, cliquez sur le buton "changer de version" puis patientez :

<figure><img src="../.gitbook/assets/image (4) (1) (1) (1) (1) (1) (1) (1).png" alt=""><figcaption><p>Après cela, démarez votre serveur.</p></figcaption></figure>
