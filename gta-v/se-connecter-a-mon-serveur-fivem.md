---
description: Vous trouverez ici le tutoriel pour se connecter à votre serveur FiveM
---

# 🖱️ Se connecter à mon serveur FiveM

## **Étape** 1 : Trouver l'adresse IP ou le nom du serveur

Pour commencer, vous devez connaître le nom de votre serveur ou votre adresse IP. Pour cela, rendez-vous sur le dashboard du serveur et trouvez en bas "Adresse IP" :

<figure><img src="../.gitbook/assets/image (4) (1) (1).png" alt=""><figcaption></figcaption></figure>

Une fois que vous avez, trouvé l'adresse IP ou configuré un [nom de domaine](../dashboard/mettre-un-nom-de-domaine-sur-son-serveur.md), rendez-vous sur FiveM.

### Trouver le serveur sur le jeu par le nom

Ouvrez FiveM, une fois ceci fait cliquez sur "Jouer"&#x20;

<figure><img src="../.gitbook/assets/image (1) (1) (1) (1) (1).png" alt=""><figcaption></figcaption></figure>

Par la suite, entrez le nom de votre serveur.&#x20;

<figure><img src="../.gitbook/assets/image (2) (1) (1) (1) (1).png" alt=""><figcaption><p>V</p></figcaption></figure>

Vous pouvez désormez, cliquez sur le bouton "Se connecter".&#x20;

### Trouver le serveur sur le jeu par l'adresse IP&#x20;

Appuyez sur votre touche F8 du menu principal de FiveM. Par la suite, écrivez "connect VOTREIP:PORT" \
Voici un exemple&#x20;

<figure><img src="../.gitbook/assets/image (3) (1) (1) (1).png" alt=""><figcaption></figcaption></figure>

Enfin, vous pouvez appuyer sur entrer pour pouvoir vous connecter :)&#x20;
