---
description: >-
  Vous retrouverez ici le tutoriel pour ajouter votre collection à votre
  serveur.
---

# 📚 Ajouter sa collection au serveur

## **Étape 1** : Trouver sa collection

Pour commencer, il faut faire une collection, nous partons du principe que vous l'avez déjà crée, il faut donc récupérer les derniers chiffres de la collection à la fin de l'url steam de votre collection.

<figure><img src="../.gitbook/assets/Screenshot_786.png" alt=""><figcaption><p>Où trouver l'id de sa collection steam</p></figcaption></figure>

## **Étape 2** : Configurer son serveur

Pour continuer, il faudra vous rendre dans la section "Paramètres" du dashboard, et remplir "Workshop ID" par l'id copié juste avant. Vous n'avez plus qu'a démarrer votre serveur.

<figure><img src="../.gitbook/assets/Screenshot_787.png" alt=""><figcaption><p>Comment renseigner son Workshop ID</p></figcaption></figure>

#### Félicitation 🎉 ! Vous venez de configurer la collection pour votre serveur !
