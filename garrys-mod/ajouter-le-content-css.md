---
description: Vous retrouverez ici le tutoriel pour ajouter le content css à votre jeu.
---

# ⚠ Ajouter le content CSS

## **Étape 1** : Télécharger le content

Veuillez vous rendre sur le site [gmodcontent.com](https://gmodcontent.com/) et télécharger le Counter Strike: Source Game Content. Vous obtiendrez un fichier .zip.

<figure><img src="../.gitbook/assets/Screenshot_788.png" alt=""><figcaption><p>Où télécharger le dossier content</p></figcaption></figure>

## **Étape 2** : Installer le content

Dézipper l’archive que vous venez de télécharger et copier le dossier css-content-gmod directement dans le dossier addons de votre serveur (accessible via FTP, sur votre dashboard OxygenServ).

#### Félicitation 🎉 ! Vous venez d'ajouter le content CSS !
