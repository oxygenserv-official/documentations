---
description: >-
  Vous apprendrez à travers ce tuto à modifier l'argent de départ sur votre
  serveur Nova-Life.
---

# 💰 Modifier l'argent de départ sur mon serveur Nova-Life

## Étape 1 : Accéder aux fichiers

Pour commencer, il faut avoir un accès aux fichiers du serveur. Pour cela, rendez-vous dans l'onglet "FTP”:

<figure><img src="../.gitbook/assets/image.png" alt=""><figcaption></figcaption></figure>

## Étape 2 : Trouver le ficher

Aller dans le dossier "Servers" :

<figure><img src="../.gitbook/assets/image (48).png" alt=""><figcaption></figcaption></figure>

Puis dans le dossier "NovaLifeServer" :

<figure><img src="../.gitbook/assets/image (51).png" alt=""><figcaption></figcaption></figure>

Et enfin, dans le dossier "Config" :

<figure><img src="../.gitbook/assets/image (1).png" alt=""><figcaption></figcaption></figure>

Puis ouvrir "roleplay.json":

<figure><img src="../.gitbook/assets/image (46).png" alt=""><figcaption></figcaption></figure>

## Étape 3 : Modifier le ficher

Une fois dans "roleplay.json" trouver la partie du code suivante:&#x20;

```
"startMoney":500,"startBank":0
```

<figure><img src="../.gitbook/assets/image (47).png" alt=""><figcaption></figcaption></figure>



{% hint style="info" %}
"startMoney": Argent de départ en poche à la création du personnage,\
"startBank": Argent de départ en banque à la création du personnage.
{% endhint %}

Et voilà il vous reste plus que à modifier les nombre, voici un exemple:&#x20;

```
"startMoney":200,"startBank":3000
```

{% hint style="info" %}
Si le nombre est plus grand que "999" par exemple: "2 000", "925 000", "9 325 250",  il ne faut pas mettre d'espace entre les nombre, exemple avec les nombres précédents: "2000", "925000", "9325250".
{% endhint %}

## Félicitation 🎇 ! <a href="#felicitation" id="felicitation"></a>

Vous avez désormais modifier l'argent de base sur votre serveur !
