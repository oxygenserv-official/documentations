---
description: Vous apprendrez dans ce tuto à avoir les permissions administrateur.
---

# 🛡️ Être administrateur sur mon serveur Nova Life



### Les logiciels prérequis:

[DB Brower](https://sqlitebrowser.org)

## **Étape** 1 : Accéder aux fichiers

Pour commencer, il faut avoir un accès aux fichiers du serveur. Cliquez sur l'onglet "FTP" :

<figure><img src="../.gitbook/assets/image (1) (1) (1) (1) (1) (1).png" alt=""><figcaption></figcaption></figure>

### Etape 2: Se connecter au serveur

Il faut se connecter au moins une fois pour être dans la base de données du serveur, et ensuite pouvoir modifier les informations.



### Etape 3: Télécharger le fichier de la base de données

<figure><img src="../.gitbook/assets/image (3) (1) (1) (1) (1).png" alt=""><figcaption></figcaption></figure>

Une fois le fichier téléchargé sur votre ordinateur, ouvrez le avec DB Browser.

## **Étape** 2 : Modifier son adminlevel

Une fois sur la base de données, rendez-vous dans l'onglet "_Parcourir les données_", où vous trouverez tous les joueurs ayant rejoins votre serveur.

{% hint style="info" %}
Si vous n'y aparaissez pas, vous devez vous connecter au moins une fois au serveur, puis ressayer.
{% endhint %}

Dans la colonne "adminlevel", choisissez le niveau administratrateur du joueur (entre 0 et 5), 5 étant le niveau maximal :

<figure><img src="../.gitbook/assets/image (4) (1) (1) (1) (1).png" alt=""><figcaption></figcaption></figure>

N'oubliez pas d'enregistrer le fichier.

## **Étape 3** : Réuploader les fichier dans le serveur

Vous pouvez renvoyer votre fichier au serveur.

Faites "Uploader".

<figure><img src="../.gitbook/assets/image (6) (1).png" alt=""><figcaption></figcaption></figure>



<figure><img src="../.gitbook/assets/image (5) (1) (1).png" alt=""><figcaption></figcaption></figure>

Sélectionnez le fichier dans votre ordinateur et faites "Ouvrir".

Une notification en bas à droite vous informe que le fichier à bien été envoyé.

Redémarrez le serveur pour que les modification prennet effet.

Une fois arrivé sur la partie, il faut faire F5 pour accéder au menu administrateur.

#### Félicitation 🎉 ! Vous êtes maintenant administrateur de votre serveur Nova Life !
