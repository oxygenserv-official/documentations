---
description: >-
  Vous apprendrez à travers ce tuto à installer un plugin sur votre serveur
  Nova-Life.
---

# 💾 Installer un plugin sur mon serveur Nova-Life

## Étape 1 : Accéder aux fichiers

Pour commencer, il faut avoir un accès aux fichiers du serveur. Pour cela, rendez-vous dans l'onglet "FTP”:

<figure><img src="../.gitbook/assets/ftp.png" alt=""><figcaption></figcaption></figure>

## Étape 2 : Trouver un ou plusieurs plugins

De nombreux plugins sont disponibles sur le discord de “MOD” : [https://discord.gg/7dYDGGvxHK](https://discord.gg/7dYDGGvxHK)

## Étape 3 : Télécharger le plugin

Il vous faudra télécharger le fichier “.DLL”:

<figure><img src="../.gitbook/assets/Dl ddl.png" alt=""><figcaption><p>Pour télécharger un fichier sur “GITHUB”, il faut cliquer sur le texte en bleu.</p></figcaption></figure>

L'enregistrer à l’endroit souhaité sur votre ordinateur :

<figure><img src="../.gitbook/assets/save ddl.png" alt=""><figcaption></figcaption></figure>

{% hint style="info" %}
ATTENTION : Le fichier peut être détecté comme un virus par votre navigateur internet, il affichera alors une erreur
{% endhint %}

<figure><img src="../.gitbook/assets/pasted image 0.png" alt=""><figcaption></figcaption></figure>

Voici comment quand même le télécharger : cliquez sur les 3 petits points, puis cliquez sur "Conserver" :

<figure><img src="../.gitbook/assets/conserver.png" alt=""><figcaption></figcaption></figure>

Puis cliquez sur "Afficher plus":

<figure><img src="../.gitbook/assets/afficher plus.png" alt=""><figcaption></figcaption></figure>

Puis sur "Conserver quand même" :

<figure><img src="../.gitbook/assets/Conserver quand même.png" alt=""><figcaption></figcaption></figure>

Et voilà, le fichier devrait normalement se télécharger !

## Étape 4 : Uploader son plugin sur le serveur

Retourner dans le “FTP” comme expliqué dans l’étape 1, aller dans le dossier "Servers" :

<figure><img src="../.gitbook/assets/image (50).png" alt=""><figcaption></figcaption></figure>

Puis dans le dossier "NovaLifeServer" :

<figure><img src="../.gitbook/assets/image (52).png" alt=""><figcaption></figcaption></figure>

Et enfin, dans le dossier "Plugins" :

<figure><img src="../.gitbook/assets/image (53).png" alt=""><figcaption></figcaption></figure>

Cliquez sur le bouton "Uploader" :

Puis sélectionner le fichier que vous avez téléchargé sur votre ordinateur :

<figure><img src="../.gitbook/assets/pasted image 0 (1).png" alt=""><figcaption></figcaption></figure>

Vérifier que votre plugin est bien dans la liste :

<figure><img src="../.gitbook/assets/image (55).png" alt=""><figcaption></figcaption></figure>

## Étape 5 : Relancer le serveur

Vous pouvez maintenant démarrer ou redémarrer votre serveur pour que les modifications soient appliquées.

## Félicitation 🎇 !

Vous avez désormais un plugin sur votre serveur !
