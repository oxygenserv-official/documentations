# Table of contents

* [👋 Bienvenue chez Oxygenserv](README.md)

## 🌍 Dashboard

* [📂 Accéder aux fichiers de son serveur](dashboard/acceder-aux-fichiers-de-son-serveur.md)
* [👥 Ajouter des sous-utilisateurs sur son serveur](dashboard/ajouter-des-sous-utilisateurs-sur-son-serveur.md)
* [📋 Planifier une nouvelle tâche](dashboard/planifier-une-nouvelle-tache.md)
* [💾 Créer une sauvegarde et la restaurer sur son serveur](dashboard/creer-une-sauvegarde-et-la-restaurer-sur-son-serveur.md)
* [📝 Créer et accéder à sa base de données](dashboard/creer-et-acceder-a-sa-base-de-donnees.md)
* [💿 Créer une sauvegarde de base de données sur son serveur](dashboard/creer-une-sauvegarde-de-base-de-donnees-sur-son-serveur.md)
* [🛡️ Changer le mot de passe de son compte](dashboard/changer-le-mot-de-passe-de-son-compte.md)
* [📑 Trouver et partager ses logs serveur](dashboard/trouver-et-partager-ses-logs-serveur.md)
* [💬 Mettre un nom de domaine sur son serveur](dashboard/mettre-un-nom-de-domaine-sur-son-serveur.md)
* [📞 Contacter le support (discord et dashboard)](dashboard/contacter-le-support-discord-et-dashboard.md)
* [🐯 Mettre son serveur en hibernation](dashboard/mettre-son-serveur-en-hibernation.md)
* [🤖 Inviter OxyBot et le configurer sur son discord](dashboard/inviter-oxybot-et-le-configurer-sur-son-discord.md)

## 🐤 Palworld

* [🖥️ Se connecter à mon serveur Palworld](palworld/se-connecter-a-mon-serveur-palworld.md)
* [🛡️ Être administrateur sur mon serveur Palworld](palworld/etre-administrateur-sur-mon-serveur-palworld.md)
* [💻 Transférer une sauvegarde Co-Op (locale) vers Oxygenserv](palworld/transferer-une-sauvegarde-co-op-locale-vers-oxygenserv.md)
* [🖥️ Transférer une sauvegarde d'un hébergeur vers Oxygenserv](palworld/transferer-une-sauvegarde-dun-hebergeur-vers-oxygenserv.md)
* [💻 Transférer une sauvegarde Windows vers Linux](palworld/transferer-une-sauvegarde-windows-vers-linux.md)

## 🦏 ARK: Survival Ascended

* [⚙️ Installer un serveur ARK: Survival Ascended](ark-survival-ascended/installer-un-serveur-ark-survival-ascended.md)
* [🖥️ Se connecter à mon serveur ARK: Survival Ascended sur PC et console](ark-survival-ascended/se-connecter-a-mon-serveur-ark-survival-ascended-sur-pc-et-console.md)
* [📅 Transférer son serveur ARK: Survival Ascended sur Oxygenserv](ark-survival-ascended/transferer-son-serveur-ark-survival-ascended-sur-oxygenserv.md)

## 🎮 Nova Life

* [🛡️ Être administrateur sur mon serveur Nova Life](nova-life/etre-administrateur-sur-mon-serveur-nova-life.md)
* [💾 Installer un plugin sur mon serveur Nova-Life](nova-life/installer-un-plugin-sur-mon-serveur-nova-life.md)
* [💰 Modifier l'argent de départ sur mon serveur Nova-Life](nova-life/modifier-largent-de-depart-sur-mon-serveur-nova-life.md)

## 🚔 GTA V

* [🖱️ Se connecter à mon serveur FiveM](gta-v/se-connecter-a-mon-serveur-fivem.md)
* [⚙️ Installer un serveur FiveM avec TXAdmin](gta-v/installer-un-serveur-fivem-avec-txadmin.md)
* [↗️ Changer de version d'Artificat](gta-v/changer-de-version-dartificat.md)
* [😤 Corriger les erreurs "1";"2";"126";"127" et "134"](gta-v/corriger-les-erreurs-1-2-126-127-et-134.md)

## 🏰 Enshrouded

* [🖥️ Se connecter à mon serveur Enshrouded](enshrouded/se-connecter-a-mon-serveur-enshrouded.md)

## 🔫 Arma 3

* [🖥️ Se connecter à mon serveur Arma 3](arma-3/se-connecter-a-mon-serveur-arma-3.md)
* [🛡️ Être administrateur sur mon serveur Arma 3](arma-3/etre-administrateur-sur-mon-serveur-arma-3.md)
* [➕ Autres tutoriels](arma-3/autres-tutoriels.md)

## ⛏️ Minecraft

* [🛡️ Être administrateur sur mon serveur Minecraft](minecraft/etre-administrateur-sur-mon-serveur-minecraft.md)
* [📚 Installer Spigot / Bukkit / Forge](minecraft/installer-spigot-bukkit-forge.md)
* [⚙️ Modifier les réglages de son serveur](minecraft/modifier-les-reglages-de-son-serveur.md)
* [🚧 Activer la whitelist sur ton serveur](minecraft/activer-la-whitelist-sur-ton-serveur.md)
* [📄 Personnaliser son MOTD dans la liste des serveurs](minecraft/personnaliser-son-motd-dans-la-liste-des-serveurs.md)
* [🥳 Installer des plugins](minecraft/installer-des-plugins.md)

## 🕶️ Garry's Mod

* [📚 Ajouter sa collection au serveur](garrys-mod/ajouter-sa-collection-au-serveur.md)
* [⚠️ Ajouter le content CSS](garrys-mod/ajouter-le-content-css.md)
