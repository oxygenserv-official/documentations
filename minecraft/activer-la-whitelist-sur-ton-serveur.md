---
description: >-
  Vous retrouverez ici le tutoriel pour activer la whitelist sur son serveur
  Minecraft.
---

# 🚧 Activer la whitelist sur ton serveur

## **Étape 1** : Activer la whitelist

Pour activer la whitelist, il faut se rendre, sur la section "Paramètres" de son serveur, puis cliquer sur "Activer la whitelist sur votre serveur".

<figure><img src="../.gitbook/assets/Screenshot_666.png" alt=""><figcaption><p>Où trouver le bouton "Activer la whitelist"</p></figcaption></figure>

## **Étape 2** : Ajouter quelqu'un en whitelist

Pour ajouter quelqu'un, il faut faire la commande `whitelist add [Pseudo]` dans l'onglet console.

<figure><img src="../.gitbook/assets/Screenshot_665.png" alt=""><figcaption></figcaption></figure>

#### Félicitation 🎉 ! Vous venez d'activer la whitelist et de la configurer sur votre serveur Minecraft !
