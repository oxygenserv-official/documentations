---
description: Vous retrouverez ici le tutoriel pour installer de plugins sur votre serveur
---

# 🥳 Installer des plugins

## **Étape 1** : Choisir un plugin

Pour commencer, il faut trouver son plugin, je vais donc me rendre sur [spigotmc.org](https://www.spigotmc.org/) et rechercher un plugin, mon choix se penche sur [DecentHolograms](https://www.spigotmc.org/resources/decentholograms-1-8-1-20-papi-support-no-dependencies.96927/). Je cliquer ensuite sur le bouton "Download Now" en vérifiant bien que ma version est compatible.

<figure><img src="../.gitbook/assets/Screenshot_784.png" alt=""><figcaption><p>Où trouver un plugin</p></figcaption></figure>

## **Étape 2** : Mettre le plugin sur son serveur

Je me rends ensuite dans la section "FTP" sur le dashboard, puis, une fois dans le dossier "Plugins", je clique sur "Uploader". Vous n'avez plus qu'a redémarrer votre serveur, et voilà ! Votre plugin est bien installé.

<figure><img src="../.gitbook/assets/Screenshot_785.png" alt=""><figcaption><p>Comment installer son plugin</p></figcaption></figure>

#### Félicitation 🎉 ! Vous venez d'installer un plugin sur votre serveur !
