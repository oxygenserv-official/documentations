---
description: >-
  Vous retrouverez ici le tutoriel pour personnaliser votre MOTD sur la
  configuration de votre serveur.
---

# 📄 Personnaliser son MOTD dans la liste des serveurs

## **Étape 1** : Aller dans la section "Paramètres"

Pour commencer, il faut se rendre dans la partie "Paramètres" de votre dashboard. Ensuite, vous devrez regarder la case "MOTD", et le modifier ici.

<figure><img src="../.gitbook/assets/Screenshot_782 (1).png" alt=""><figcaption><p>Où trouver le MOTD</p></figcaption></figure>

## **Étape 2** : Modifier le MOTD

Pour modifier le MOTD, il faut en générer un, vous pouvez vous rendre sur [ce site](https://minecraft.tools/fr/motd.php) pour en générer un facilement.

<figure><img src="../.gitbook/assets/Screenshot_783.png" alt=""><figcaption><p>Comment générer son MOTD</p></figcaption></figure>

#### Félicitation 🎉 ! Vous venez de configurer votre MOTD pour votre serveur !
