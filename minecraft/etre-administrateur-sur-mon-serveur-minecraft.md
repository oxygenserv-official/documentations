---
description: Vous retrouverez ici le tutoriel pour se donner les droits sur serveur.
---

# 🛡 Être administrateur sur mon serveur Minecraft

## **Étape 1** : Accéder à la section "Console"

Pour commencer, vous devrez vous rendre dans la section "Console" du dashboard.

<figure><img src="../.gitbook/assets/Screenshot_621.png" alt=""><figcaption><p>Où trouver la section "Console" du dashboard</p></figcaption></figure>

## **Étape 2** : Taper la commande

Ensuite, vous devez executer la commande "**`/op [pseudo]`**" en précisant votre pseudo et appuyer sur Entrée.

<figure><img src="../.gitbook/assets/Screenshot_622.png" alt=""><figcaption><p>Comment executer la commande</p></figcaption></figure>

#### Félicitation 🎉 ! Vous venez de vous donner les droits admin sur votre serveur !
