---
description: Vous retrouverez ici le tutoriel pour modifier les régles de votre serveur.
---

# ⚙ Modifier les réglages de son serveur

## **Étape 1** : Accéder à la section "FTP" et au fichier "server.properties"

Pour commencer, il faut accéder à la section FTP du dashboard, et trouver le fichier "server.properties", puis l'ouvrir.

<figure><img src="../.gitbook/assets/Screenshot_628.png" alt=""><figcaption><p>Où trouver la section "FTP"</p></figcaption></figure>

## **Étape 2** : Modifier les paramètres

Il y a plusieurs paramètre importants à changer pour un serveur, cela dépend du type de serveur que vous voulez faire.\
\
`gamemode` permet de changer le mode de jeu par défaut quand quelqu'un se connecte\
`enable-command-block` permet d'activer les blocks de commandes\
`level-name` défini le nom du dossier qui sera utilisé pour le monde par défaut\
`motd` message affiché dans la liste des serveurs [cliquez ici](https://minecraft.tools/fr/motd.php) pour en générer un\
`pvp` active ou non le pvp sur le serveur\
`difficulty` met la difficulté par défaut sur le serveur\
`online-mode` active ou non, le mode qui ne permet d'avoir que des comptes Minecraft premium\
`view-distance` indique le nombre de la distance de vue (impacte grandement les performances)

Pour en générer un automatiquement, vous pouvez [cliquer ici](https://mctools.org/server-properties-creator)\
\
Pour finir cliquez sur "Sauvegarder".

<figure><img src="../.gitbook/assets/Screenshot_629.png" alt=""><figcaption><p>Comment configurer le "server.properties"</p></figcaption></figure>

#### Félicitation 🎉 ! Vous venez de modifier les paramètres pour votre serveur !
