---
description: >-
  Vous retrouverez ici le tutoriel pour installer une version spécifique de
  Minecraft
---

# 📚 Installer Spigot / Bukkit / Forge

## **Étape 1** : Accéder à la section "Installation"

Pour commencer, veuillez vous rendre dans la section "Installation" du dashboard.

<figure><img src="../.gitbook/assets/Screenshot_623.png" alt=""><figcaption><p>Où trouver la section "Installation"</p></figcaption></figure>

## **Étape 1** : Installer une version

Pour installer une nouvelle version, vous devez cliquer sur le nom de la version qui vous plaît, sa version de Minecraft, sa version de Java (Certaines versions de minecraft nécessitent une version Java spécifique), et sa version de Spigot, Paper, Bukkit, ou encore Forge.

<figure><img src="../.gitbook/assets/Screenshot_624.png" alt=""><figcaption><p>Comment installer sa version</p></figcaption></figure>

#### Félicitation 🎉 ! Vous venez d'Installer une version Minecraft pour votre serveur !
