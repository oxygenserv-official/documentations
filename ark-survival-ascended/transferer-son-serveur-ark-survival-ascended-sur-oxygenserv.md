---
description: >-
  Vous trouverez ici le tutoriel pour migrer votre serveur ARK Survival Ascended
  de Nitrado vers OxygenServ.
---

# 📅 Transférer son serveur ARK: Survival Ascended sur Oxygenserv



## 🖥 Migrer son serveur Nitrado vers Oxygenserv

### **Étape** 1 : Préparer les dossiers sur Oxygenserv

Pour commencer votre migration dès votre achat sur Oxygenserv, vous devez au moins démarrer votre serveur une première fois pour y créer les dossiers nécessaires.

Une fois cela fait, éteignez-le pour pouvoir le modifier sans contrainte.

### **Étape** 2 : Récupérer vos réglages sur Nitrado

#### 1) Localisation des fichiers

1.

La localisation des fichiers des réglages se fait sur le menu "Navigateur de fichiers (ou "File Browser" si notre Nitrado est en anglais).

*

<figure><img src="../.gitbook/assets/image02.png" alt=""><figcaption></figcaption></figure>

Une fois dans le navigateur de fichier, suivez le chemin suivant pour arriver à vos réglages:

<figure><img src="../.gitbook/assets/image03.png" alt=""><figcaption></figcaption></figure>

Nous pouvons à présent télécharger les fichiers nécessaires.

#### 2) Téléchargement des fichiers

Une fois sur le dossier "Config", il vous suffira de cliquer sur le bouton téléchargement du fichier Game.ini et GameUserSettings.ini .

<figure><img src="../.gitbook/assets/image04.png" alt=""><figcaption></figcaption></figure>

Ceci à présent, vous aurez les 2 fichiers sur votre ordinateur.

<figure><img src="../.gitbook/assets/image_2024-01-21_123900600.png" alt=""><figcaption></figcaption></figure>

Il est temps à présent de les importer sur Oxygenserv.

### **Étape 3**: Importer vos réglages Nitrado sur Oxygenserv

#### 1) Supprimer les réglages par défaut

Le serveur d'Oxygenserv a généré des réglages de base mais nous n'en avons pas besoin ici. Il faut donc les supprimer pour y importer ceux de Nitrado.

Rendons-nous dans le dossier "ShooterGame > Saved > Config > **WindowsServer**" sur Oxygenserv.

<figure><img src="../.gitbook/assets/image06.png" alt=""><figcaption></figcaption></figure>

A présent, localisez les 2 fichiers.

<figure><img src="../.gitbook/assets/image07.png" alt=""><figcaption></figcaption></figure>

Cliquez sur le bouton supprimer pour les effacer de votre serveur.

#### 2) Importer les fichiers .ini sur Oxygenserv.

Il est temps de mettre en ligne les fichiers .ini sur Oxygenserv. Pour cela, il vous faut cliquer sur le bouton "Uploader".

<figure><img src="../.gitbook/assets/Retouche 08.png" alt=""><figcaption></figcaption></figure>

Vous ne pourrez mettre que en ligne un fichier à la fois. Il faudra donc répéter l'envoi une deuxième fois.

Vos fichiers sont à présent en ligne sur Oxygenserv.

### **Étape 4**: Importer les sauvegardes sur Oxygenserv

#### 1) Récupérer vos sauvegardes sur Nitrado

Pour récupérer vos sauvegardes Nitrado, il vous faudra vous rendre dans le dossier "SavedArks" du navigateur de fichier.

Vous constaterez le dossier "The Island" à l'intérieur. Actuellement c'est normal vu qu'il n'y a qu'une seule map jouable, ce qui ne sera pas forcément le cas plus tard.

<figure><img src="../.gitbook/assets/image09.png" alt=""><figcaption></figcaption></figure>

Une fois dans le dossier de votre map (en l'occurence ici The Island), vous devrez trouver un fichier "TheIsland\_WP.ark". Ce fichier est la dernière sauvegarde faite par le jeu.

<figure><img src="../.gitbook/assets/image10.png" alt=""><figcaption></figcaption></figure>

Si vous voulez des sauvegardes plus récentes en plus, vous devrez les rechercher selon leur date et heure dans ce même dossier.

<figure><img src="../.gitbook/assets/image11.png" alt=""><figcaption></figcaption></figure>

Exemple: Le fichier "TheIsland\_WP\_08.01.2024\_18.31.56.ark.gz" .

<figure><img src="../.gitbook/assets/image12.png" alt=""><figcaption></figcaption></figure>

Il représente la sauvegarde fait par le jeu le 8 Janvier 2024 à 18h31sec56.

#### 2) Importer les sauvegardes sur Oxygenserv

Concernant l'importation, vous serez face à 2 situations :

* Votre sauvegarde fait moins que 100 Mo (100.000 Ko)
* Votre sauvegarde fait plus que 100 Mo (100.00 Ko)

Avant d'aller plus loin, n'oubliez pas de supprimer la sauvegarde faite par Oxygenserv.

Rendons-nous dans le dossier "ShooterGame > Saved > SavedArks > **TheIsland\_WP**" et supprimez "**TheIsland\_WP.ark**"

<figure><img src="../.gitbook/assets/Retouche 13.png" alt=""><figcaption></figcaption></figure>

→ Si votre sauvegarde **fait moins** que 100 Mo, vous pourrez simplement l'uploader via Oxygenserv en utilisant le bouton "Uploader".

→ Si votre sauvegarde **fait plus** que 100 Mo, vous devrez utiliser un logiciel utilisant le protocole FTP (Par exemple FileZilla) afin de vous connecter à Oxygenserv.

<figure><img src="../.gitbook/assets/image_2024-01-21_130515360.png" alt=""><figcaption></figcaption></figure>

Une fois sur FileZilla (par exemple), introduisez les identifiants de votre compte Oxygenserv dans les bons champ afin d'établir la liaison.

<figure><img src="../.gitbook/assets/Retouche 15.png" alt=""><figcaption></figcaption></figure>

Quand vous allez cliquer sur le bouton connexion, vous risquerez d'avoir une fenêtre d'avertissement. Cliquez sur OK sans crainte.

_**Astuce : Le champ "Hôte" de FileZilla est votre identifiant "IP"**_

Vous êtes désormais connecté à FileZilla. Maintenant, rendez-vous dans le dossier de votre map (ShooterGame > Saved > SavedArks > **TheIsland\_WP**) comme si vous étiez sur Oxygenserv.

<figure><img src="../.gitbook/assets/Retouche 19.png" alt=""><figcaption></figcaption></figure>

A présent, glissez votre fichier .ark dans le dossier "**TheIsland\_WP**" encadré en rouge ci-dessus. Une barre de téléchargement apparaitra en dessous. Elle sera suivi par un fenêtre popup qui atteste que le fichier a bien été téléchargé sur le serveur.

Vous pouvez à présent fermer FileZilla.

### **Étape 5**: Vérification finale sur Oxygenserv

Maintenant que vos réglages et votre (ou vos) sauvegarde(s) sont sur Oxygenserv, procédez à une dernière vérification de vos paramètres généraux dans la section "Paramètres" comme par exemple l'ID de vos mods pour éviter un conflit de migration.

### **Étape 6**: Lancement de votre serveur sur Oxygenserv

Maintenant que toutes les préparations sont faites, vous pouvez lancer votre serveur sur Oxygenserv.

###

**Félicitation 🎉 ! Vous venez de migrer votre serveur ARK Survival Ascended de Nitrado vers OxygenServ. Il est temps de retourner apprivoiser des dinosaures !**
