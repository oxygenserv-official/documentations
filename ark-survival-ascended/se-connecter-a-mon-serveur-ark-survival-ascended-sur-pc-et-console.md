---
description: >-
  Découvrez notre guide complet pour se connecter facilement à votre serveur
  ARK: Survival Ascended, compatible avec PC, PS5 et Xbox.
---

# 🖥 Se connecter à mon serveur ARK: Survival Ascended sur PC et console

> Les étapes à suivre sont identiques, indépendamment de la plateforme utilisée.

## **Étape** 1 : Trouver le nom du serveur

Pour commencer, vous devez connaître le nom de votre serveur. Pour cela, rendez-vous sur l'onglet "Paramètres" et trouvez "Nom du serveur" :

<figure><img src="../.gitbook/assets/image (11).png" alt=""><figcaption></figcaption></figure>

Une fois que vous avez, trouvé le nom du serveur, rendez-vous sur Ark.

{% hint style="warning" %}
Assurez vous que votre serveur et bien "en ligne" sur le dashboard avant de continuer.
{% endhint %}

### **Étape** 2 : Afficher la liste des serveurs sur le jeu

Cliquez sur "Join game" pour voir la liste des serveurs :

<figure><img src="../.gitbook/assets/image (13).png" alt=""><figcaption></figcaption></figure>

### **Étape 3** : Filter les serveurs dans la liste

1. Rendez-vous dans la catégorie "Unofficial"
2. Entrez le nom de votre serveur (ou une partie)
3. Si votre serveur à un mot de passe, cochez la case "Show password protected servers"
4. **Dans tous les cas, cochez la case "Show player servers"**
5. Si votre serveur n'apparaît pas dans la liste, cliquez sur le bouton "Refresh"

<figure><img src="../.gitbook/assets/image (12).png" alt=""><figcaption></figcaption></figure>

### **Étape 4** : Créer votre personnage et amusez-vous !

Après avoir rejoins le serveur, vous pouvez créer votre personnage et commencer à jouer :

<figure><img src="../.gitbook/assets/image (14).png" alt=""><figcaption></figcaption></figure>

#### Félicitation 🎉 ! Vous venez de rejoindre votre serveur Ark !
