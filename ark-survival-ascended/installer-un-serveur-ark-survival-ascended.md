---
layout:
  title:
    visible: true
  description:
    visible: false
  tableOfContents:
    visible: true
  outline:
    visible: true
  pagination:
    visible: true
---

# ⚙️ Installer un serveur ARK: Survival Ascended

{% hint style="danger" %}
En vous faisant cela, vous confirmez que l'utilisation de 'ARK: Survival Ascended Server' sur notre plateforme Ark Server Cloud est destinée à un usage strictement personnel et non commercial, conformément aux conditions d'utilisation de Studio Wildcard. Toute utilisation commerciale est interdite par les termes de la licence du jeu.
{% endhint %}

## **Étape** 1 : Accéder à l'onglet Installation

Cliquez sur l'onglet "Installation" sur le dashboard de votre serveur :

<figure><img src="../.gitbook/assets/image (1) (1) (1) (1) (1) (1) (1).png" alt=""><figcaption></figcaption></figure>

## **Étape 2** : Choisissez la version de ARK à installer

<figure><img src="../.gitbook/assets/image (1) (1) (1) (1) (1) (1) (1) (1).png" alt=""><figcaption></figcaption></figure>

## **Étape 3** : Connectez-vous à votre compte Steam

{% hint style="info" %}
Cette étape est une validation nécessaire demandé par ARK pour installer le jeu. Vos identifiants ne sont pas sauvegardés et nous n'en aurons pas connaissance.
{% endhint %}

<figure><img src="../.gitbook/assets/image (2) (1) (1) (1) (1) (1) (1).png" alt=""><figcaption></figcaption></figure>

Une fois vos identifiants entrés, cliquez sur le bouton "Installer" :

<figure><img src="../.gitbook/assets/image (3) (1) (1) (1) (1) (1).png" alt=""><figcaption><p>Une fois ceci fait, votre serveur sera en cours d'installation !</p></figcaption></figure>

Vous pouvez à présent modifier les paramètres de votre serveur ARK dans l'onglet "Paramètres" !&#x20;

{% hint style="info" %}
Pour plus d'informations à propos de l'installation et des paramètres, voici une vidéo de notre partenaire ASFAX qui vous explique cela :\
[Configurer sa partie solo ou son serveur Ark Ascended Tuto !](https://www.youtube.com/watch?v=xVbZdbHJQso\&t=494s\&ab\_channel=Asfax)
{% endhint %}

#### Félicitation 🎉 ! Vous venez d'Installer votre serveur ARK: Survival Ascended !
