---
description: Vous apprendrez dans ce tuto à avoir les permissions administrateur.
---

# 🛡 Être administrateur sur mon serveur Palworld



## **Étape** 1 : Trouver le mot de passe admin

Pour commencer, il faut avoir le mot de passe admin de votre serveur. Cliquez sur l'onglet "Paramètres" puis prenez la valeur "Mot de passe admin" :

<figure><img src="../.gitbook/assets/image (20).png" alt=""><figcaption></figcaption></figure>

{% hint style="info" %}
Si vous n'avez pas changé le mot de passe, je vous conseille très fortement de le faire. Vous pouvez mettre le texte de votre choix.
{% endhint %}

## **Étape** 2 : Se connecter en admin

Vous pouvez maintenant vous connecter au jeu, ouvrir le chat (touche "Entrée) et taper la commande : /AdminPassword \<mot de passe> :

<figure><img src="../.gitbook/assets/image (22).png" alt=""><figcaption></figcaption></figure>

Si votre mot de passe est correct, vous devriez avoir ce message de confirmation :

<figure><img src="../.gitbook/assets/image (21).png" alt=""><figcaption></figcaption></figure>

## Étape 3 : Utiliser les commandes admin

Vous pouvez maintenant utiliser les commandes administrateur dans le chat, voici la liste :

| Commande                       | Description                                                                      |
| ------------------------------ | -------------------------------------------------------------------------------- |
| /Shutdown {Secondes} {Message} | Arrête le serveur pour tout le monde une fois que le temps imparti s'est écoulé. |
| /DoExit                        | Arrête le serveur.                                                               |
| /KickPlayer {Steam ID}         | Expulse le joueur du serveur.                                                    |
| /BanPlayer {Steam ID}          | Banni le joueur du serveur.                                                      |
| /Broadcast {Message}           | Affiche un message à tous les joueurs du serveur.                                |
| /TeleportToPlayer {Steam ID}   | Vous téléporte au joueur.                                                        |
| /TeleportToMe {Steam ID}       | Téléporte le joueur vers vous.                                                   |
| /ShowPlayers                   | Affiche la liste des joueurs connectés au serveur.                               |
| /Info                          | Affiche les informations du serveur.                                             |
| /Save                          | Sauvegarde les données du monde.                                                 |

#### Félicitation 🎉 ! Vous êtes maintenant administrateur de votre serveur Palworld !
