---
description: >-
  Vous trouverez ici le tutoriel pour transférer vos données d'un serveur
  Palworld sous Windows vers un serveur Palworld sous Linux.
---

# 💻 Transférer une sauvegarde Windows vers Linux

{% embed url="https://github.com/xNul/palworld-host-save-fix?tab=readme-ov-file#how-to-migrate-a-windowslinux-dedicated-server-save-to-a-linuxwindows-dedicated-server" %}

***

### **Étape 1** : Transférer les fichiers et dossiers de votre partie :

Copiez ces fichiers vers votre serveur dans le dossier `Pal/Saved/SaveGames/0/XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX/` (un dossier avec 32 de caractères aléatoires) :

{% hint style="danger" %}
**Il est impératif de lancer une première fois le serveur et de se connecter en jeu afin de générer les fichiers du jeu puis d'éteindre le serveur pour toute manipulation des fichiers.**
{% endhint %}

<figure><img src="../.gitbook/assets/image (33).png" alt=""><figcaption></figcaption></figure>

{% hint style="warning" %}
**Ne copiez pas le fichier WorldOption.sav** (sinon votre config du serveur ne sera pas pris en compte)**, ni le dossier backups** (il n'est pas nécessaire).
{% endhint %}

### **Étape** 2 : Lancez votre serveur et créez un nouveau personnage  :

Connectez-vous à votre serveur pour générer un nouveau personnage (que nous allons transférer ensuite) :

<figure><img src="../.gitbook/assets/image (34).png" alt=""><figcaption></figcaption></figure>

{% hint style="success" %}
A cette étape, vous devriez remarquer que votre map a bien été transférée. Nous allons maintenant transférer les données des joueurs.
{% endhint %}

### **Étape 3** : Re-téléchargez les fichiers du serveur sur votre PC  :

Commencez par éteindre le serveur, comme avant toute modification.

Dans votre FTP, retournez dans `Pal/Saved/SaveGames/0/XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX/` et téléchargez les fichiers sur votre PC (dans un nouveau dossier) :

<figure><img src="../.gitbook/assets/image (38).png" alt=""><figcaption></figcaption></figure>

{% hint style="success" %}
Dans le dossier "players" vous devriez remarquer qu'il y a un nouveau fichier ".sav" par rapport à votre partie locale.
{% endhint %}

### **Étape 4 : Convertir les fichiers de données des joueurs**

Téléchargez l'application "palworld-host-save-fix" sur votre ordinateur à partir du lien suivant : [https://github.com/JannikBirn/palworld-host-save-fix/releases/download/v.0.1.2/palworld-host-save-fix.exe](https://github.com/JannikBirn/palworld-host-save-fix/releases/download/v.0.1.2/palworld-host-save-fix.exe)

{% file src="../.gitbook/assets/palworld-host-save-fix.exe" %}

{% hint style="info" %}
Il est possible que Windows ou votre anti-virus ait peur de ce fichier, vous pouvez l'ignorer et poursuivre l'installation.
{% endhint %}

Ouvrez l'application, une interface comme ceci devrait apparaître :

<figure><img src="../.gitbook/assets/image (36).png" alt=""><figcaption></figcaption></figure>

Cliquez sur le bouton "Browse" et allez dans les fichiers que vous avez téléchargé précédemment pour sélectionner le fichier "Level.sav" :

<figure><img src="../.gitbook/assets/image (39).png" alt=""><figcaption></figcaption></figure>

Une nouvelle interface apparaît, dans la première liste, sélectionnez votre ancien personnage et dans la deuxième liste, sélectionnez le personnage que vous avez créé lors de l'étape 2 (il doit être level 0 logiquement) :

<figure><img src="../.gitbook/assets/image (40).png" alt=""><figcaption></figcaption></figure>

Cliquez sur le bouton "Migrate" puis validez le message de confirmation :

<figure><img src="../.gitbook/assets/image (41).png" alt=""><figcaption></figcaption></figure>

### **Étape** 5 : Re-transférer les nouveaux fichiers sur le serveur

Comme lors de l'étape 1, transférez les nouveaux fichiers vers votre serveur dans le dossier `Pal/Saved/SaveGames/0/XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX/` :

<figure><img src="../.gitbook/assets/image (42).png" alt=""><figcaption></figcaption></figure>

Une fois le transfert terminé, vous pouvez relancer votre serveur et vous connecter pour retrouver votre sauvegarde sur le serveur :

<figure><img src="../.gitbook/assets/image (43).png" alt=""><figcaption></figcaption></figure>

#### Félicitation 🎉 ! Vous avez transféré votre sauvegarde locale vers votre serveur Palworld !

