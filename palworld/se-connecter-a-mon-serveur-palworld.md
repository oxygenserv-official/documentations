---
description: Vous trouverez ici le tutoriel pour se connecter à votre serveur Palworld.
---

# 🖥 Se connecter à mon serveur Palworld

## **Étape** 1 : Trouver l'adresse IP ou le nom du serveur

Pour commencer, vous devez connaître le nom de votre serveur ou votre adresse IP. Pour cela, rendez-vous sur le dashboard du serveur et trouvez en bas "Adresse IP" :

<figure><img src="../.gitbook/assets/image (16).png" alt=""><figcaption></figcaption></figure>

Une fois que vous avez trouvé l'adresse IP ou configuré un [nom de domaine](../dashboard/mettre-un-nom-de-domaine-sur-son-serveur.md), rendez-vous sur Palworld.

### **Étape** 2 : Trouver le serveur sur le jeu

Rendez-vous sur l'onglet "Rejoindre une partie multijoueur" :

<figure><img src="../.gitbook/assets/image (17).png" alt=""><figcaption></figcaption></figure>

#### Pour une connexion par IP (recommandé)

Renseignez l'adresse IP et le port de votre serveur dans le champ en bas. Et ensuite, cliquez-sur "Connexion" :

<figure><img src="../.gitbook/assets/image (4) (1).png" alt=""><figcaption></figcaption></figure>

{% hint style="info" %}
Si votre serveur a un mot de passe, vous devez cocher la case "Saisir un mot de passe".
{% endhint %}

#### Pour une connexion avec le nom du serveur (très long)

{% hint style="danger" %}
Palworld n'affiche que 200 serveurs à la fois dans la liste, il est très probable que vous le trouvez pas votre serveur dans la liste car elle n'est pas afifchée entièrement.

Vous devez afficher tous les serveurs avant de faire la recherche, et donc cliquer de nombreuses fois sur "Afficher les 200 suivants".
{% endhint %}

Allez dans l'onglet "Serveurs communautaires" :

<figure><img src="../.gitbook/assets/image (19).png" alt=""><figcaption></figcaption></figure>

#### Félicitation 🎉 ! Vous venez de rejoindre votre serveur Palworld !
