---
description: >-
  Vous trouverez ici le tutoriel pour transférer vos données de sauvegarde
  depuis un autre hébergeur vers Oxygenserv
---

# 🖥️ Transférer une sauvegarde d'un hébergeur vers Oxygenserv

### **Étape 1** : Téléchargez les fichiers et dossiers à transférer depuis l'hébergeur :

Rendez-vous sur le dashboard de gestion de fichiers ou le FTP de votre serveur de jeu chez l'hébergeur et téléchargez les dossiers et fichiers suivants sur votre poste :

* `Pal\Saved\SaveGames\0\XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\` un dossier avec pleins de chiffres(32) devrait s'y trouver, ce dossier contient les données de sauvegarde qui nous intéressent&#x20;
* `Pal\Saved\Config\LinuxServer\PalWorldSettings.ini` Ce fichier correspond à la configuration de votre serveur, ce fichier est optionnel si vous n'avez modifié aucune rates.

### **Étape** 2 : Transferez les fichiers de sauvegarde du monde vers [votre serveur Palworld Oxygenserv](https://www.oxygenserv.com/location/serveur-palworld/)  :

{% hint style="danger" %}
**Il est impératif de lancer une première fois le serveur et de se connecter en jeu afin de charger les fichiers de configuration et de sauvegarde puis d'éteindre le serveur pour toute manipulation des fichiers.**
{% endhint %}

[Ne transférez pas le dossier ayant un nom super long, seulement les fichiers à l'intérieur de ce dossier.](#user-content-fn-1)[^1]

Rendez-vous sur le FTP depuis le dashboard d'Oxygenserv de votre serveur Palworld :

<figure><img src="../.gitbook/assets/FTP Oxygenserv.png" alt=""><figcaption></figcaption></figure>

Rendez-vous dans **/Pal/Saved/SaveGames/0/XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX/**

<figure><img src="../.gitbook/assets/image (25).png" alt=""><figcaption></figcaption></figure>

Remplacez les deux fichiers et le dossier Players par ceux présents dans votre sauvegarde en faisant un glisser déposer.

{% hint style="danger" %}
Ne transférez pas le dossier ayant un nom super long, seulement les fichiers à l'intérieur de ce dossier.
{% endhint %}

### **Étape 3** : Transferez le fichier de configuration du serveur vers Oxygenserv  :

Rendez-vous dans **/Pal/Saved/SaveGames/Config/LinuxServer/** et remplacez le fichier PalWorldSettings.ini en faisant un glisser déposer votre fichier depuis votre poste

<figure><img src="../.gitbook/assets/FTP Oxygenserv configuration.png" alt=""><figcaption></figcaption></figure>

Une fois la manipulation réalisée, vous pouvez relancer votre serveur et vous connecter pour retrouver votre sauvegarde, Enjoy !

[^1]: 
